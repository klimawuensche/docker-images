.PHONY: build-%
BASE_REPO := $(file < base.repo)

# manually build, test (put not push) the image. Set PULL_PARENTS='true' environment variable to force image pull on build
build-%:
	cd "images/$*" && DOCKER_BASE_REPO="${BASE_REPO}" bash ../../scripts/build